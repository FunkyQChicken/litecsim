/* ENGR-2350 SPRING 2020 - LAB0
 * SIMULATOR TEMPLATE LAB
 * */

#define RIN     xxxxxxxx                // Used to produce simulation variations
//#define PRINTTOFILE "FILENAME.csv"    // Uncomment to print to file

//#include<c8051_SDCC.h>
#include "C8051_SIM.h"
#include<stdio.h>
#include<stdlib.h>

void PCA_Init();
void Interrupt_Init();

// You can use this to replace pin sbits
#define PB  P3_1

unsigned int counts = 0;

void main(void){
    unsigned int time = 0;
    Sys_Init();
    putchar(0);
    PCA_Init();
    Interrupt_Init();

    while(1){
        Sim_Update();   // This function synchronizes the simulation and this program
                        // Sim_Update() needs to be called in EVERY LOOP
        while(counts < 5){
            Sim_Update();   // Must Call this inside ALL WHILE LOOPS!
        }
        counts = 0;
        time++;
        printf("\r\nSimulation time passed: %u TENTHS",time);
    }
}

void PCA_Init(void){
    PCA0MD |= 0x01; // SYSCLK/12, Interrupt Enable
    CR = 1; // Same as PCA0CN |= 0x40;
}

void Interrupt_Init(void){
    EIE1 |= 0x08;       // Enable PCA interrupt
    EA = 1;             // Globally Enable interrupts
}

void PCA_ISR(void){
    if(CF){     // If a PCA overflow has occurred
        CF = 0;         // Clear the interrupt overflow flag
        PCA0 = 28672;   // Preload the counter for 20 ms operation
        counts++;       // Increment *all* our counters
    }else{   // If a CCM interrupt triggered the PCA ISR, clear and ignore.
        PCA0CN &= 0xC0;
    }
}
