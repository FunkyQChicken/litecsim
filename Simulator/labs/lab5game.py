# Template: https://www.pygame.org/docs/tut/tom_games2.html
import pygame,math
import numpy as np
from scipy.interpolate import interp2d,RegularGridInterpolator
import sys,os
from labcommon import * # @UnresolvedImport
from matplotlib import use as mpl_use 
mpl_use("Agg")
#mpl_use("pgf") # For generating TEX figure
from matplotlib import pyplot
from matplotlib.patches import Circle
import matplotlib.backends.backend_agg as agg
from version import SIMULATOR_VERSION

INFOA = "LITEC Lab 5 Simulator"
INFOB = INFOA+" Version "+SIMULATOR_VERSION

SIMSTEP = 0.01 # seconds between updates

SEL_LOC_X = 1085
RAMP_CENTER = (1300,400)

class Simulation():
    def __init__(self,controlmodel,runctl,asset_path=None):
        self.ctlmod = controlmodel
        self.runctl = runctl
        self.cfgdone = False
        # Initialize screen
        pygame.init()
        pygame.font.init()
        self.clock = pygame.time.Clock()
        self.screen = pygame.display.set_mode((1200,800))
        pygame.display.set_caption(INFOA) 
        
        # Set background
        self.background = pygame.Surface(self.screen.get_size()).convert()
        self.background.fill((225,225,225))
        
        # Get file locations
        if asset_path is None: 
            try:
                base_path = sys._MEIPASS
            except Exception:
                base_path = os.path.abspath(".")
            asset_path = base_path+"/assets/"
        
        pygame.display.set_icon(pygame.image.load(asset_path+"icon.png"))
        
        self.car = DrivingCar(asset_path,
                              scale=0.08,
                              center=(300,700),
                              angle=0,
                              simstep=SIMSTEP)  
                                    
        self.font = pygame.font.SysFont('Serif', 14)
        self.info = self.font.render(INFOB,True,(0,0,0))
        self.info_rect = self.info.get_rect()
        self.info_rect.bottomleft = (5,800)
        
        self.active_car_loc = 0        
        self.car_loc_sel = self.font.render('CAR LOCATION:',True,(0,0,0))
        self.car_locs_text = [self.font.render('TOP',True,(0,0,0)),
                              self.font.render('MID-UP',True,(0,0,0)),
                              self.font.render('MID-DOWN',True,(0,0,0)),
                              self.font.render('BOTTOM',True,(0,0,0))]
        self.car_locs_rect = []
        yloc = 23
        for car_loc in self.car_locs_text:
            self.car_locs_rect.append(car_loc.get_rect())
            self.car_locs_rect[-1].width = 1200-SEL_LOC_X
            self.car_locs_rect[-1].top = yloc
            self.car_locs_rect[-1].left = SEL_LOC_X
            yloc += 17
        
    
        self.startrect = pygame.Rect((800,800),self.car.surfsize)
        self.startangle = 0
        
        self.end = 0
        
        self.f_ramp,self.ramp = self.buildramp(center=RAMP_CENTER)
        self.tiltscaling = 5.67    # This should be between 1.73-5.67 (effectively 30 deg to 10 deg)
        
        self.measwidth,self.measheight = self.car.rect_car.size
        self.measwidth *= 0.6       
        self.measheight *= 0.6
        self.measpoints = np.array([[0,-self.measheight/2],  # Front of car
                                    [0,self.measheight/2],   # Back of car
                                    [-self.measwidth/2,0],  # Left of car
                                    [self.measwidth/2,0]])  # Right of car
        
        #calculate the maximum nominal slope           
        pnts = self.f_ramp([[0,400],[100,400]])
        self.maxslope = (pnts[1]-pnts[0])/100
        
        # Make the car faster
        self.car.Drive.maxspeed = 100
        self.car.Drive.breakspeed = 200
        self.car.Drive.maxchange = 100*SIMSTEP
        self.car.start_speed = 0.05*self.car.Drive.maxspeed    # Minimum speed at which the car needs to start
        self.car.dead_speed = 0.025*self.car.Drive.maxspeed      # Speed at which car will not cease to move
        
        #self.startup()
        
    def reset(self):
        if self.active_car_loc == 0:
            self.car.pos_y_orig = 100
            self.car.angle_orig = -np.pi
        elif self.active_car_loc == 1:
            self.car.pos_y_orig = 400
            self.car.angle_orig = 0
        elif self.active_car_loc == 2:
            self.car.pos_y_orig = 400
            self.car.angle_orig = -np.pi
        else:
            self.car.pos_y_orig = 700
            self.car.angle_orig = 0
        self.car.reset()
        self.end = 0
        
        # Don't reset SlideSwitches, let state propogate
        #for SS in self.SS:
        #    SS.reset()  
        
    def update(self):
        # Update peripheral timing
        self.ctlmod.timestep(SIMSTEP)
        
        # Update mechanical components
        self.car.Servo.setdc(self.ctlmod.xbr.getpin(1,0,'CCM'),self.ctlmod.pca0.Tperiod)
        self.car.Drive.setdc(self.ctlmod.xbr.getpin(1,2,'CCM'),self.ctlmod.pca0.Tperiod)
        
        self.settilt()
        
        # Move the car
        self.car.update()
            
        # report sensor values
        self.sendaccel()
        
    def sendaccel(self):
        # Nominally, a "maxslope" (-1) pitch should correspond to 20 tilt. Vary around 10-30
        x_theta = np.arctan(self.car.roll/self.tiltscaling)
        x_accel = np.sin(x_theta)*1000  # Units milli-g
        y_theta = np.arctan(self.car.pitch/self.tiltscaling)
        y_accel = np.sin(y_theta)*1000 + self.car.accel/0.00981     # Add car acceleration into y accel - 
        z_accel = np.sqrt(1000000-x_accel**2-y_accel**2)
        #print('{}\t{}\t{}'.format(x_accel,y_accel,z_accel))
        #print('{}\t{}'.format(np.sin(y_theta)*1000,self.car.accel/0.00981))
        randomscale = 15-self.tiltscaling
        if not np.isnan(x_accel):
            self.ctlmod.accel.setaccel(x_accel=x_accel+np.random.normal(0,scale=randomscale))
        if not np.isnan(y_accel):
            self.ctlmod.accel.setaccel(y_accel=y_accel+np.random.normal(0,scale=randomscale))
        if not np.isnan(z_accel):
            self.ctlmod.accel.setaccel(z_accel=z_accel+np.random.normal(0,scale=randomscale))
        #self.ctlmod.accel.setaccel(x_accel,y_accel,z_accel)
            
    def checkdone(self):
        if not self.ramp.get_rect().collidepoint(self.car.center()):
            self.end = -2
        return bool(self.end)
    
    def setconfig(self,cfg):
        # Need to generate:
        #    Drive resistance - self.car.dead_speed - between 0.05x and 0.15x self.Drive.maxspeed
        #    Slope            - self.tiltscaling - between 10-30 degrees
        #
        
        #self.car.dead_speed = ((cfg & 0x0F)/150 + 0.05)*self.car.Drive.maxspeed
        #self.tiltscaling = ((cfg >> 4) & 0x0F)/15+10        # Calculates a degrees value
        #self.tiltscaling = 1/np.arctan(np.radians(self.tiltscaling))    # Gets the scaling factor
        #print('TiltScaling: {}'.format(self.tiltscaling))
        
        self.tiltscaling = 5.367412133037435 # Just give a value here, I don't want to debug variations
        self.cfgdone = True
        
    
    def blit(self):
        #self.screen.blit(self.background,(0,0))
        self.screen.blit(self.ramp,(0,0))
        
        self.car.draw(self.screen)
            
        pygame.draw.rect(self.screen,(245,245,245),self.info_rect.inflate(20,5))
        self.screen.blit(self.info,self.info_rect)
        
        
        pygame.draw.rect(self.screen,(245,245,245),pygame.Rect(SEL_LOC_X-5,0,400,self.car_locs_rect[-1].bottom+4))
        self.screen.blit(self.car_loc_sel,(SEL_LOC_X,2))
        pygame.draw.rect(self.screen,(190,190,190),self.car_locs_rect[self.active_car_loc].inflate(2,2))
        for car_locs,rects in zip(self.car_locs_text,self.car_locs_rect):
            self.screen.blit(car_locs,rects)
        
        font = pygame.font.SysFont('Serif', 30,bold=True)
        if not self.cfgdone:
            endrect = pygame.Rect((0,0),(400,100))
            endrect.center = (600,400)
            endtext1 = font.render('RIN NOT PROVIDED',True,(0,0,0))
            endtext1_rect = endtext1.get_rect(center=endrect.center)
            endtext1_rect.bottom = endrect.centery-3
            endtext2 = font.render('#define RIN xxxxxxxxx',True,(255,0,0))
            endtext2_rect = endtext2.get_rect(center=endrect.center)
            endtext2_rect.top = endrect.centery+3
            pygame.draw.rect(self.screen,(0,0,0),endrect,width=5)
            pygame.draw.rect(self.screen,(255,255,255),endrect)
            self.screen.blit(endtext1,endtext1_rect)
            self.screen.blit(endtext2,endtext2_rect)
        
    def buildramp(self,center=(0,0),shading=True,contour=True):
        x = np.linspace(0,1200,1200)
        y = np.linspace(0,800,800)
        xx,yy = np.meshgrid(x,y)
        flat = 100
        transition = 100
        slope_div = .5        
        
        r_inner = 200
        r_inner2 = r_inner-transition
        r_outer = r_inner+flat
        r_outer2 = r_outer+transition
        
        radius = np.sqrt((xx-center[0])**2+(yy-center[1])**2)
        
#         m1 = -0.5
#         b1 = 0
#         m2 = -1
#         b2 = m1*(r_outer2-r_outer)-m2*(r_outer2-r_outer)
#         
#         radius = np.sqrt((xx-center[0])**2+(yy-center[1])**2)
#          
#         conditions =  [radius < r_inner2,
#                       (r_inner2  <= radius) & (radius < r_inner),
#                       (r_inner  <= radius) & (radius < r_outer),
#                       (r_outer  <= radius) & (radius < r_outer2),
#                       r_outer2 <= radius ]
#                        
#         height_functions = [lambda r: -m2*(r-r_inner2)-b2,
#                             lambda r: -m1*(r-r_inner),
#                             lambda r: 0,
#                             lambda r: m1*(r-r_outer),
#                             lambda r: m2*(r-r_outer2)-b2]
#          
#         slope_functions = [lambda r: m2,
#                            lambda r: m1,
#                            lambda r: 0,
#                            lambda r: m1,
#                            lambda r: m2]

        m = -2*(r_outer2-r_outer)/slope_div
        b = -(r_outer2-r_outer)**2/slope_div - m*(r_outer2-r_outer)
         
        radius = np.sqrt((xx-center[0])**2+(yy-center[1])**2)
         
        conditions =  [radius < r_inner2,
                      (r_inner2  <= radius) & (radius < r_inner),
                      (r_inner  <= radius) & (radius < r_outer),
                      (r_outer  <= radius) & (radius < r_outer2),
                      r_outer2 <= radius ]
                       
        height_functions = [lambda r: -m*(r-r_inner2)-b,
                            lambda r: -(r-r_inner)**2/slope_div,
                            lambda r: 0,
                            lambda r: -(r-r_outer)**2/slope_div,
                            lambda r: m*(r-r_outer2)-b]
         
        slope_functions = [lambda r: m,
                           lambda r: 2*(r-r_inner)/slope_div,
                           lambda r: 0,
                           lambda r: -2*(r-r_outer)/slope_div,
                           lambda r: m]
        
        height = np.piecewise(radius,conditions,height_functions)
        min_height = np.abs(np.min(height))
        height = height/min_height
        
        slope = np.piecewise(radius,conditions,slope_functions)
        
        fig = pyplot.figure(figsize=(12,8))
        ax = fig.add_axes([0,0,1,1])
        ax.spines['right'].set_color('none')
        ax.spines['left'].set_color('none')
        ax.spines['top'].set_color('none')
        ax.spines['bottom'].set_color('none')
        # turn off ticks
        ax.xaxis.set_ticks_position('none')
        ax.yaxis.set_ticks_position('none')
        ax.xaxis.set_ticklabels([])
        ax.yaxis.set_ticklabels([])

        #https://subscription.packtpub.com/book/game_development/9781782162865/1/ch01lvl1sec13/using-matplotlib-with-pygame-simple
        canvas = agg.FigureCanvasAgg(fig)
        
        pyplot.imshow(slope,cmap='Blues_r',alpha=0.5)
        inc = 0.025
        minor_lines = np.arange(-1+inc/2,0,inc)
        major_lines = np.arange(-1,0,inc)
        pyplot.contour(x,y,height,colors='grey',linestyles='solid',levels=minor_lines)
        pyplot.contour(x,y,height,colors='grey',linestyles='dashed',levels=major_lines)
        #pyplot.contour(x,y,height,colors='black',linestyles='solid',levels=np.linspace(-1,-b/min_height,20))
        ax.autoscale(enable=False)
        pyplot.gca().add_patch(Circle((center[0],center[1]),r_outer*1.075,facecolor='none',edgecolor='black',linewidth=2,linestyle='dashed'))
        pyplot.gca().add_patch(Circle((center[0],center[1]),r_inner,facecolor='none',edgecolor='black',linewidth=2,linestyle='dashed'))
        
        canvas.draw()
        renderer = canvas.get_renderer()
        raw_data = renderer.tostring_rgb()
        size = canvas.get_width_height()
        #return (interp2d(x,y,height,kind='linear'),
        return (RegularGridInterpolator((x,y),np.transpose(height),method='linear',bounds_error=False),
                pygame.image.fromstring(raw_data,size,"RGB"))
    
    def settilt(self):
        anglesin = np.sin(self.car.angle)
        anglecos = np.cos(self.car.angle)
        txmatrix = np.array([[anglecos,anglesin],[-anglesin,anglecos]])
        c_to_points = np.transpose(txmatrix.dot(np.transpose(self.measpoints)))
        #print(c_to_points)
        points = c_to_points+np.array(self.car.center())
        
        # Get height at points
        h_points = self.f_ramp(points)
        # Get and set tilts
        if not any(np.isnan(h_points)): # Don't update if we get NaNs!
            self.car.pitch = (h_points[0]-h_points[1])/self.measheight  # Front to back
            self.car.roll =  (h_points[3]-h_points[2])/self.measwidth  # Right to left
            self.car.pitch /= self.maxslope
            self.car.roll /= self.maxslope
        
        #print('{}\t{}'.format(self.car.pitch,self.car.roll))
        
            
    def run(self):
        while self.runctl > 0:
            if self.runctl == 2:
                self.runctl.run = 1
                self.reset()
            if self.runctl > 2:
                self.setconfig(self.runctl.run)
                self.runctl.run = 1
                self.reset()
                
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    return
                elif event.type == pygame.MOUSEBUTTONDOWN:
                    pos = pygame.mouse.get_pos()
                    for i,rect in enumerate(self.car_locs_rect):
                        if rect.collidepoint(pos):
                            self.active_car_loc = i
                elif event.type == pygame.MOUSEBUTTONUP:
                    pass
            
            if not self.checkdone():
                self.update()
                self.blit()
            else:
                self.blit()
                font = pygame.font.SysFont('Serif', 30,bold=True)
                if self.end == -2:
                    endrect = pygame.Rect((0,0),(400,100))
                    endrect.center = (600,400)
                    endtext = font.render('OUT OF BOUNDS',True,(255,0,0))
                    pygame.draw.rect(self.screen,(0,0,0),endrect,width=5)
                    pygame.draw.rect(self.screen,(255,255,255),endrect)
                    self.screen.blit(endtext,endtext.get_rect(center=endrect.center))

            pygame.display.flip()

            self.clock.tick(1/SIMSTEP)
            
    
            
        
if __name__ == "__main__":
    #sim = Simulation(0,1,'../assets/')
    #sim.run()
    
    x = np.linspace(0,1200,1200)
    y = np.linspace(0,800,800)
    xx,yy = np.meshgrid(x,y)
    flat = 100
    transition = 100
    slope_div = .5        
    
    r_inner = 200
    r_inner2 = r_inner-transition
    r_outer = r_inner+flat
    r_outer2 = r_outer+transition

#     m = -2*(r_outer2-r_outer)/slope_div
#     b = -(r_outer2-r_outer)**2/slope_div - m*(r_outer2-r_outer)
#     
#     radius = np.sqrt((xx-centerx)**2+(yy-centery)**2)
#     
#     conditions =  [radius < r_inner2,
#                   (r_inner2  <= radius) & (radius < r_inner),
#                   (r_inner  <= radius) & (radius < r_outer),
#                   (r_outer  <= radius) & (radius < r_outer2),
#                   r_outer2 <= radius ]
#                   
#     height_functions = [lambda r: -m*(r-r_inner2)-b,
#                         lambda r: -(r-r_inner)**2/slope_div,
#                         lambda r: 0,
#                         lambda r: -(r-r_outer)**2/slope_div,
#                         lambda r: m*(r-r_outer2)-b]
#     
#     slope_functions = [lambda r: -m,
#                        lambda r: 2*(r-r_inner)/slope_div,
#                        lambda r: 0,
#                        lambda r: -2*(r-r_outer)/slope_div,
#                        lambda r: m]

    m = -2*(r_outer2-r_outer)/slope_div
    b = -(r_outer2-r_outer)**2/slope_div - m*(r_outer2-r_outer)
     
    radius = np.sqrt((xx-RAMP_CENTER[0])**2+(yy-RAMP_CENTER[1])**2)
     
    conditions =  [radius < r_inner2,
                  (r_inner2  <= radius) & (radius < r_inner),
                  (r_inner  <= radius) & (radius < r_outer),
                  (r_outer  <= radius) & (radius < r_outer2),
                  r_outer2 <= radius ]
                   
    height_functions = [lambda r: -m*(r-r_inner2)-b,
                        lambda r: -(r-r_inner)**2/slope_div,
                        lambda r: 0,
                        lambda r: -(r-r_outer)**2/slope_div,
                        lambda r: m*(r-r_outer2)-b]
     
    slope_functions = [lambda r: m,
                       lambda r: 2*(r-r_inner)/slope_div,
                       lambda r: 0,
                       lambda r: -2*(r-r_outer)/slope_div,
                       lambda r: m]
    
    height = np.piecewise(radius,conditions,height_functions)
    min_height = np.abs(np.min(height))
    height = height/min_height
    
    slope = np.piecewise(radius,conditions,slope_functions)

    
    pyplot.rcParams.update({
        "font.family": "serif",
        "font.serif": [],                    # use latex default serif font
        "font.sans-serif": ["DejaVu Sans"],  # use a specific sans-serif font
        "font.size":12,
    })
    
    fig = pyplot.figure(figsize=(6,3),dpi=300)
    pyplot.plot(x,height[400,:])
    #pyplot.imshow(slope,cmap='Blues_r')
    inc = 0.025
    minor_lines = np.arange(-1+inc/2,0,inc)
    major_lines = np.arange(-1,0,inc)
    #pyplot.contour(x,y,height,colors='grey',linestyles='solid',levels=minor_lines)
    #pyplot.contour(x,y,height,colors='grey',linestyles='dashed',levels=major_lines)
    #pyplot.gca().add_patch(Circle((centerx,centery),r_outer,facecolor='none',edgecolor='black',linewidth=2,linestyle='dashed'))
    #pyplot.gca().add_patch(Circle((centerx,centery),r_inner,facecolor='none',edgecolor='black',linewidth=2,linestyle='dashed'))
    pyplot.xlabel("Horizontal Pixel Location (Vertical = 400)")
    pyplot.ylabel("Relative Ramp Height")
    
    pyplot.tight_layout(0)
    
    pyplot.draw()
    #pyplot.show()
    pyplot.savefig('ramp_profile.pgf')
    sys.exit()
        
    
                
        
        
        
        
