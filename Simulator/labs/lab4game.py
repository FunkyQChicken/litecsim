# Template: https://www.pygame.org/docs/tut/tom_games2.html
import pygame,math
import numpy as np
from scipy.interpolate import interp1d
import sys,os
from labcommon import * # @UnresolvedImport
from version import SIMULATOR_VERSION

INFOA = "LITEC Lab 4 Simulator"
INFOB = INFOA+" Version "+SIMULATOR_VERSION

SIMSTEP = 0.01 # seconds between updates

class Simulation():
    def __init__(self,controlmodel,runctl,asset_path=None):
        self.ctlmod = controlmodel
        self.runctl = runctl
        self.cfgdone = False
        # Initialize screen
        pygame.init()
        pygame.font.init()
        self.clock = pygame.time.Clock()
        self.screen = pygame.display.set_mode((800,800))
        pygame.display.set_caption(INFOA)
        
        # Set background
        self.background = pygame.Surface(self.screen.get_size()).convert()
        self.background.fill((225,225,225))
        
        # Get file locations
        if asset_path is None:
            try:
                base_path = sys._MEIPASS
            except Exception:
                base_path = os.path.abspath(".")
            asset_path = base_path+"/assets/"
        
        pygame.display.set_icon(pygame.image.load(asset_path+"icon.png"))
        
        self.car = DrivingCar(asset_path,
                              scale=0.08,
                              center=(400,250),
                              angle=90,
                              simstep=SIMSTEP)
        self.obstacle = Obstacle(radius=15,center=(400,400),freq=0.25,amp=20)
        
        self.font = pygame.font.SysFont('Serif', 14)
        self.info = self.font.render(INFOB,True,(0,0,0))
        self.info_rect = self.info.get_rect()
        self.info_rect.bottomleft = (5,800)
        
        ss_path = asset_path + '/switch.png'
        self.SS = []
        self.SS.append(SlideSwitch(ss_path,
                                   val=0,
                                   center=(745,25),
                                   scale=1,
                                   labels=True,
                                   axis='x',
                                   port=3,
                                   pin=0))
        
        
        self.PB = Pushbutton(asset_path+'/pb-unpressed.png',
                             asset_path+'/pb-pressed.png',
                             val=1,
                             center=(745,75),
                             scale=1,
                             axis='x',
                             port=0,
                             pin=0)
    
        self.startrect = pygame.Rect((800,800),self.car.surfsize)
        self.startangle = 0
        self.endrect = self.startrect.copy()
        self.endtext = self.font.render("GOAL",True,(0,0,0))
        self.rect_endtext = self.endtext.get_rect()
        
        self.end = 0
        
        self.angleoffset = interp1d([0,100,200,300,400,500,600,700,800],
                                    [0.05,-0.53,0.29,-0.1,-0.19,-0.11,-0.38,-0.17,-0.06],
                                    #kind='cubic',
                                    bounds_error=False,
                                    fill_value="extrapolate")
        
        self.compassnorth = pygame.image.load(asset_path+"compassnorth.png").convert_alpha()
        self.compassnorth_rect = self.compassnorth.get_rect()
        self.compassnorth_rect.center = (400,50)
        self.compassnorth_rot = self.compassnorth.copy()
        self.compassnorth_rect_rot = self.compassnorth_rect.copy()
        self.compassnorth_hit = False
        
        self.startup()
        
        
    def startup(self):
        self.car.Servo.angle = 10
        self.car.Servo.desiredangle = 10
        self.car.Drive.speed = 0
        self.car.Drive.desiredspeed = 100
        self.car.Drive.initd = True
        
    def reset(self):
        self.car.reset()
        self.obstacle.reset()
        self.end = 0
        
        self.compassnorth_rot = self.compassnorth.copy()
        self.compassnorth_rect_rot = self.compassnorth_rect.copy()
        self.compassnorth_hit = False
        
        # Don't reset SlideSwitches, let state propogate
        #for SS in self.SS:
        #    SS.reset()  
        
    def update(self):
        # Update peripheral timing
        self.ctlmod.timestep(SIMSTEP)
        
        # Update mechanical components
        self.car.Servo.setdc(self.ctlmod.xbr.getpin(1,0,'CCM'),self.ctlmod.pca0.Tperiod)
        self.car.Drive.setdc(self.ctlmod.xbr.getpin(1,2,'CCM'),self.ctlmod.pca0.Tperiod)
        
        # Move the car
        self.car.update()
        
        # Check if obstacle moving is enabled and move
        if self.obstacle.run:
            if not self.ctlmod.actuator.status:
                self.obstacle.turnoff()
        else:
            if self.ctlmod.actuator.status:
                self.obstacle.turnon(self.car.center())
        self.obstacle.update(looktowards=self.car.center())
        
        # Emit the switch values
        for SS in self.SS:
            self.ctlmod.xbr.setpin(SS.port,SS.pin,SS.val)
        # Emit the PB value
        self.ctlmod.xbr.setpin(self.PB.port,self.PB.pin,self.PB.val)
            
        # report sensor values
        if self.startangle % np.radians(180):
            angleoffset = self.angleoffset(self.car.pos_y)
        else:
            angleoffset = self.angleoffset(self.car.pos_x)
            
        self.ctlmod.compass.setdirection(self.car.getangle()+angleoffset)
        self.ctlmod.ranger.setecho(self.car.detectobstacle())
        self.ctlmod.actuator.setspeed(0) # I'm not implementing this now
        self.ctlmod.actuator.setdirection(self.obstacle.angle)
        
        # update compass indicator
        if self.compassnorth_hit:
            self.adjustcompass()
    
    def checkdone(self):
        if self.endrect.collidepoint(self.car.center()):
            self.end = 1
        elif self.car.collidecirc(self.obstacle.rot_rect.center,self.obstacle.radius):
            self.end = -1
        elif not self.car.rect.colliderect(self.background.get_rect()):
            self.end = -2
        return bool(self.end)
            
    def setconfig(self,cfg):
        # Need to generate:
        #    a start wall
        #    b start loc/direction
        #    c end loc
        #    d PB pin
        #
        #    x x x x x x x x
        #    a a b b c d d d
        #          e e
        
        wall = (cfg >> 6) & 0x03    # 0-N,1-E,2-S,3-W
        sloc = (cfg >> 4) & 0x03    # 0-lcor,1-lcen,2-rcen,3-rcor
        eloc = (cfg >> 3) & 0x01    # 0-lcen,1-rcen
        self.PB.setcfg(2,cfg & 0x07)  # Pin on Port 2
        
        marginA = self.startrect.width/2
        marginB = self.startrect.height/2
        sloc_select = [marginB,800/3,2*800/3,800-marginB]
        eloc_select = [800/3,2*800/3]
        
        if wall == 0:
            self.startrect.center = (sloc_select[sloc],marginA)
            self.endrect.center = (eloc_select[eloc],800-marginA)
            if self.startrect.centerx < 400:
                self.startangle = -90
            else:
                self.startangle = 90
        elif wall == 1:
            self.startrect.center = (800-marginA,sloc_select[sloc])
            self.endrect.center = (marginA,eloc_select[eloc])
            if self.startrect.centery < 400:
                self.startangle = 180
            else:
                self.startangle = 0
        elif wall == 2:
            self.startrect.center = (sloc_select[sloc],800-marginA)
            self.endrect.center = (eloc_select[eloc],marginA)
            if self.startrect.centerx < 400:
                self.startangle = -90
            else:
                self.startangle = 90
        else:
            self.startrect.center = (marginA,sloc_select[sloc])
            self.endrect.center = (800-marginA,eloc_select[eloc])
            if self.startrect.centery < 400:
                self.startangle = 180
            else:
                self.startangle = 0
                
        if self.startangle % 180:
            self.startrect.size = self.startrect.size[-1::-1]
            self.endrect.size = self.startrect.size
        
        self.startangle = np.radians(self.startangle)
        self.rect_endtext.center = self.endrect.center
        
        # Set default car stuff
        self.car.angle_orig = self.startangle
        self.car.pos_x_orig = self.startrect.centerx
        self.car.pos_y_orig = self.startrect.centery
        
        # Move the slideswitch and PB if necessary
        if self.SS[0].rect.colliderect(self.startrect):
            self.SS[0].adjustcenter((745,800-25))
            self.PB.adjustcenter((745,800-75))
        
        self.cfgdone = True
        
    def adjustcompass(self):
        pos = pygame.mouse.get_pos()
        if self.startangle % np.radians(180):
            angleoffset = self.angleoffset(pos[1])
        else:
            angleoffset = self.angleoffset(pos[0])
        self.compassnorth_rot = pygame.transform.rotozoom(self.compassnorth,math.degrees(angleoffset),1)
        self.compassnorth_rect_rot = self.compassnorth_rot.get_rect()
        self.compassnorth_rect_rot.center = pos
        
        
    
    def blit(self):
        self.screen.blit(self.background,(0,0))
        
        self.car.draw(self.screen)
        
        if self.cfgdone:
            #pygame.draw.rect(self.screen,(255,255,255),self.startrect,width=3)
            pygame.draw.rect(self.screen,(255,255,255),self.endrect,width=3)
            self.screen.blit(self.endtext,self.rect_endtext)
        
        self.obstacle.draw(self.screen)
        
        for SS in self.SS:
            SS.draw(self.screen)
        self.PB.draw(self.screen)
            
        self.screen.blit(self.info,self.info_rect)
        
        font = pygame.font.SysFont('Serif', 30,bold=True)
        if not self.cfgdone:
            endrect = pygame.Rect((0,0),(400,100))
            endrect.center = (400,600)
            endtext1 = font.render('RIN NOT PROVIDED',True,(0,0,0))
            endtext1_rect = endtext1.get_rect(center=endrect.center)
            endtext1_rect.bottom = endrect.centery-3
            endtext2 = font.render('#define RIN xxxxxxxxx',True,(255,0,0))
            endtext2_rect = endtext2.get_rect(center=endrect.center)
            endtext2_rect.top = endrect.centery+3
            pygame.draw.rect(self.screen,(0,0,0),endrect,width=5)
            pygame.draw.rect(self.screen,(255,255,255),endrect)
            self.screen.blit(endtext1,endtext1_rect)
            self.screen.blit(endtext2,endtext2_rect)
        
        self.screen.blit(self.compassnorth_rot,self.compassnorth_rect_rot)
        
        
            
    def run(self):
        while self.runctl > 0:
            if self.runctl == 2:
                self.runctl.run = 1
                self.reset()
            if self.runctl > 2:
                self.setconfig(self.runctl.run)
                self.runctl.run = 1
                self.reset()
            
            if not self.checkdone():
                self.update()
                    
                for event in pygame.event.get():
                    if event.type == pygame.QUIT:
                        pygame.quit()
                        return
                    elif event.type == pygame.MOUSEBUTTONDOWN:
                        pos = pygame.mouse.get_pos()
                        for SS in self.SS:
                            if SS.rect.collidepoint(pos):
                                SS.toggle()
                        if self.PB.rect.collidepoint(pos):
                            self.PB.press()
                        if self.compassnorth_rect_rot.collidepoint(pos):
                            self.compassnorth_hit = True
                    elif event.type == pygame.MOUSEBUTTONUP:
                        self.PB.release()
                        self.compassnorth_hit = False
                        
                self.blit()
                
            else:
                for event in pygame.event.get():
                    if event.type == pygame.QUIT:
                        pygame.quit()
                        return
                    
                font = pygame.font.SysFont('Serif', 30,bold=True)
                if self.end == 1:
                    endrect = pygame.Rect((0,0),(400,100))
                    endrect.center = (400,200)
                    endtext = font.render('COURSE COMPLETE',True,(0,200,0))
                    pygame.draw.rect(self.screen,(0,0,0),endrect,width=5)
                    pygame.draw.rect(self.screen,(255,255,255),endrect)
                    self.screen.blit(endtext,endtext.get_rect(center=endrect.center))
                elif self.end == -1:
                    endrect = pygame.Rect((0,0),(300,100))
                    endrect.center = (400,200)
                    endtext = font.render('CRASH!',True,(255,0,0))
                    pygame.draw.rect(self.screen,(0,0,0),endrect,width=5)
                    pygame.draw.rect(self.screen,(255,255,255),endrect)
                    self.screen.blit(endtext,endtext.get_rect(center=endrect.center))
                elif self.end == -2:
                    endrect = pygame.Rect((0,0),(400,100))
                    endrect.center = (400,200)
                    endtext = font.render('OUT OF BOUNDS',True,(255,0,0))
                    pygame.draw.rect(self.screen,(0,0,0),endrect,width=5)
                    pygame.draw.rect(self.screen,(255,255,255),endrect)
                    self.screen.blit(endtext,endtext.get_rect(center=endrect.center))

            pygame.display.flip()

            self.clock.tick(1/SIMSTEP)
                
            
        
if __name__ == "__main__":
    sim = Simulation(0,1,'../assets/')
    sim.run()
        
        
        
